// Utilities
import { make } from 'vuex-pathify'

import axios from 'axios'

// Globals
import { IN_BROWSER } from '@/util/globals'

const state = {
  dark: false,
  drawer: {
    image: 0,
    gradient: 0,
    mini: false,
  },
  gradients: [
    'rgba(0, 0, 0, .7), rgba(0, 0, 0, .7)',
    'rgba(228, 226, 226, 1), rgba(255, 255, 255, 0.7)',
    'rgba(244, 67, 54, .8), rgba(244, 67, 54, .8)',
  ],
  images: [
    'https://demos.creative-tim.com/material-dashboard-pro/assets/img/sidebar-1.jpg',
    'https://demos.creative-tim.com/material-dashboard-pro/assets/img/sidebar-2.jpg',
    'https://demos.creative-tim.com/material-dashboard-pro/assets/img/sidebar-3.jpg',
    'https://demos.creative-tim.com/material-dashboard-pro/assets/img/sidebar-4.jpg',
  ],
  notifications: [],
  rtl: false,
}

const mutations = make.mutations(state)

const actions = {
  fetch: ({ commit }) => {
    const local = localStorage.getItem('vuetify@user') || '{}'
    const user = JSON.parse(local)

    for (const key in user) {
      commit(key, user[key])
    }

    if (user.dark === undefined) {
      commit('dark', window.matchMedia('(prefers-color-scheme: dark)'))
    }
  },
  update: ({ state }) => {
    if (!IN_BROWSER) return

    localStorage.setItem('vuetify@user', JSON.stringify(state))
  },
  LOGIN: ({ state }, payload) => {
    return new Promise((resolve, reject) => {
      axios
      .post('http://192.168.0.120/toes/api/travel/loginuser.php', JSON.stringify(payload))
      .then(({ data, status }) => {
        if (status === 200) {
          const user = {
            empid: data.emp_id,
            name: data.fullName,
            section: data.section,
            divisoin: data.section,
            acct_type: data.acct_type,
          }
          localStorage.setItem('hasAccessRights', 1)
          localStorage.setItem('user', JSON.stringify(user))
          resolve(true)
        } else {
          console.log('could not login')
        }
      })
      .catch(error => {
          reject(error)
      })
    })
  },
  LOGOUT: ({ commit }) => {
    localStorage.removeItem('hasAccessRights')
    localStorage.removeItem('user')
  },
}

const getters = {
  dark: (state, getters) => {
    return (
      state.dark ||
      getters.gradient.indexOf('255, 255, 255') === -1
    )
  },
  gradient: state => {
    return state.gradients[state.drawer.gradient]
  },
  image: state => {
    return state.drawer.image === '' ? state.drawer.image : state.images[state.drawer.image]
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
