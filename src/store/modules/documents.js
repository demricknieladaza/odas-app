import { commit, make } from 'vuex-pathify'

import axios from 'axios'
import { filter } from 'lodash'

const state = {
    documentisLoading: false,
    myDocs: [],
    filterDocs: [],
    search: '',
}

const mutations = make.mutations(state)

const actions = {
    CREATEDOCUMENT: ({ state }, payload) => {
        commit('documents/documentisLoading', true)
        return new Promise((resolve, reject) => {
            axios
            .post('http://localhost:8081/api/addDocument', payload)
            .then(({ data, status }) => {
                commit('documents/documentisLoading', false)
                resolve(true)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    GETMYDOCUMENTS: ({ state }) => {
        const user = JSON.parse(window.localStorage.user)
        const office = user.section
        const empid = user.empid
        return new Promise((resolve, reject) => {
            axios
            .get('http://localhost:8081/api/getMyDocuments/' + office + '/' + empid)
            .then(({ data, status }) => {
                commit('documents/myDocs', data)
                commit('documents/filterDocs', data)
                resolve(true)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    DELETEDOCUMENT: ({ state }, payload) => {
        const data = {
            id: payload,
        }
        commit('documents/documentisLoading', true)
        return new Promise((resolve, reject) => {
            axios
            .post('http://localhost:8081/api/deleteDocument/', data)
            .then(({ res, status }) => {
                commit('documents/documentisLoading', false)
                resolve(true)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
}

const getters = {
    search: (state) => {
        // console.log(state.search)
        const data = filter(state.myDocs, (doc) => {
            return doc.document_no.includes(state.search) || doc.subject.includes(state.search) || doc.sender.includes(state.search)
        })
        state.filterDocs = data
        console.log(data)
    },
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
}
