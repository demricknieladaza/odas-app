// Imports
import Vue from 'vue'
import Router from 'vue-router'
import { trailingSlash } from '@/util/helpers'
import {
  layout,
  route,
} from '@/util/routes'

import Login from '../views/Login'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    if (to.hash) return { selector: to.hash }
    if (savedPosition) return savedPosition

    return { x: 0, y: 0 }
  },
  routes: [
    layout('Default', [
      route('Dashboard'),

      // User
      route('MyDocuments', null, 'user/mydocuments'),

      // Pages
      // route('UserProfile', null, 'components/profile'),

      // // Components
      // route('Notifications', null, 'components/notifications'),
      // route('Icons', null, 'components/icons'),
      // route('Typography', null, 'components/typography'),

      // // Tables
      // route('Regular Tables', null, 'tables/regular'),

      // // Maps
      // route('Google Maps', null, 'maps/google'),
    ]),
    {
      path: '/login',
      component: Login,
      name: 'login',
      beforeEnter: (to, from, next) => {
        const rights = window.localStorage.hasAccessRights
        if (rights !== '1') {
          next()
        } else {
          next('/')
        }
      },
    },
  ],
})

router.beforeEach((to, from, next) => {
  return to.path.endsWith('/') ? next() : next(trailingSlash(to.path))
})

export default router
